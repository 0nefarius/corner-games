<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/606467765.jpg',
            'title' => 'Battlefield V',
            'description' => 'Kolejna część bestsellerowej serii pierwszoosobowych strzelanek, za której opracowanie odpowiada studio DICE.',
            'price' => 139
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/1618.jpg',
            'title' => 'Minecraft',
            'description' => 'Minecraft na PC, X360 itd. to przebojowa produkcja niezależnego studia Mojang Specifications.',
            'price' => 109
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/13689.jpg',
            'title' => 'Wiedźmin 3: Dziki Gon',
            'description' => 'Gra action RPG, stanowiąca trzecią część przygód Geralta z Rivii.',
            'price' => 30
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/10961.jpg',
            'title' => 'Terraria',
            'description' => 'Opracowana przez niezależne studio Re-Logic sandboksowa gra platformowa, określana jako dwuwymiarowa wersja popularnego Minecrafta.',
            'price' => 35
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/11596.jpg',
            'title' => 'Borderlands 2',
            'description' => 'Pierwszoosobowa strzelanina (FPS) z elementami cRPG, stanowiąca sequel hitu wydanego w 2009 roku.',
            'price' => 38
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/568.jpg',
            'title' => 'Diablo III',
            'description' => 'Trzecia część cieszącej się ogromną popularnością serii gier RPG akcji, wykreowanej i rozwijanej przez firmę Blizzard Entertainment.',
            'price' => 59
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/15933.jpg',
            'title' => 'Cities: Skylines',
            'description' => 'Należąca do popularnego gatunku city builderów strategia ekonomiczna, za której powstanie odpowiada fińskie studio Colossal Order – twórcy cyklu Cities In Motion.',
            'price' => 29
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/15377.jpg',
            'title' => 'Darkest Dungeon',
            'description' => 'Opracowana przez debiutujące studio Red Hook gra cRPG utrzymana w stylistyce dark fantasy. Jej akcja przenosi nas do mrocznego świata wzorowanego na epoce średniowiecza.',
            'price' => 27
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/17367.jpg',
            'title' => 'Worms W.M.D',
            'description' => 'Kolejna pełnoprawna odsłona bestsellerowej serii turowych gier strategicznych, koncentrujących się na pełnej humoru rywalizacji tytułowych robali.',
            'price' => 28
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/352607812.jpg',
            'title' => 'Forza Horizon 4',
            'description' => 'Czwarta odsłona bestsellerowego cyklu gier wyścigowych z otwartym światem, tworzonych przez studio Playground Games. ',
            'price' => 208
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/534673046.jpg',
            'title' => 'Resident Evil 2',
            'description' => 'Pełnoprawny remake gry Resident Evil 2, która pojawiła się w 1998 roku na PlayStation.',
            'price' => 159
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/441646015.jpg',
            'title' => 'RAGE 2',
            'description' => 'Druga część serii postapokaliptycznych gier FPS, zapoczątkowanej przez zespół id Software.',
            'price' => 190
        ]);
        $product->save();

        $product = new \App\Product(['imagePath' => 'https://cdn.gracza.pl/galeria/gry13/grupy/668724562.jpg',
            'title' => 'F1 2019',
            'description' => 'Oparta na licencji Formuły 1 gra wyścigowa, reprezentująca serię rozwijaną przez studio Codemasters Software. ',
            'price' => 179
        ]);
        $product->save();
    }
}
