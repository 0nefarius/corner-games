(function main() {
    let stripe = Stripe('pk_test_ylP2IVZX30eF2yqCnCwsWkBN00qEDkHNiq');
    let elements = stripe.elements();

    let card = elements.create('card');
    card.mount('#card-element');

    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        let form = document.getElementById('checkout-form');
        let hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }

    function createToken() {
        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server
                stripeTokenHandler(result.token);
            }
        });
    }

// Create a token when the form is submitted.
    let form = document.getElementById('checkout-form');
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        createToken();
    });
})();