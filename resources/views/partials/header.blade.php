<nav class="navbar sticky-top navbar-expand-lg" style="background-color: #330066; height: 80px">
    <a class="navbar-brand" href="{{ route('product.index') }}">CORN'ER' GAMES <img src="https://img.icons8.com/dotty/96/ffffff/controller.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

   <div class="collapse navbar-collapse" id="navbarSupportedContent" style="font-size: 20px">
       <ul class="navbar-nav mr-auto"></ul>
       <ul class="navbar-nav mr-lg-1">
           <li class="nav-item">
               <a class="nav-link" href="{{ route('product.shoppingCart') }}" style="color: #bfbfbf">
                   <img src="https://img.icons8.com/material-rounded/48/ffffff/move-by-trolley.png">
                   Koszyk
                   <span class="badge"> ({{ Session::has('cart') ? Session::get('cart')->totalQty : ''  }})</span>
               </a>

           </li>
           <li class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" style="color: #bfbfbf" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <img src="https://img.icons8.com/material-rounded/48/ffffff/gender-neutral-user.png">
                   Profil użytkownika
               </a>
               <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="width: 100%">
                   @if(Auth::check())
                       <a class="dropdown-item" href="{{ route('user.profile') }}">Moje zamówienia</a>
                       <div class="dropdown-divider"></div>
                       <a class="dropdown-item" href="{{ route('user.logout') }}">Wyloguj</a>
                   @else
                       <a class="dropdown-item" href="{{ route('user.signin') }}">Zaloguj się</a>
                       <div class="dropdown-divider"></div>
                       <a class="dropdown-item" href="{{ route('user.signup') }}">Zarejestruj</a>
                   @endif
               </div>
            </li>
       </ul>
    </div>
</nav>

