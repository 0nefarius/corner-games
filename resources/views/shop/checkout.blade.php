@extends('layouts.master')

@section('title')
    Corn'er' Games!
@endsection

@section('content')
    <div id="checkout-container">
        <div class="row">
            <div id="checkout">
                <h1>Podsumowanie</h1>
                <h4>Suma: {{ $total }} PLN</h4>
                @if(Session::has('error'))
                    <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">
                        {{ Session::get('error') }}
                    </div>
                @endif

                <form action="{{ route('checkout') }}" method="post" id="checkout-form">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="text" id="name" placeholder="Imię i nazwisko" class="form-controll" required name="name">
                        </div>
                    </div>
                    <label for="card-element">
                        Karta kredytowa lub debetowa
                    </label>
                    <div id="card-element">
                        <!-- a Stripe Element will be inserted here. -->
                    </div>

                    <!-- Used to display form errors -->
                    <div id="card-errors" role="alert"></div>
                    {{ csrf_field() }}
                    <button type="submit" id="sc-button" style="margin-top: 30px">Zamów teraz!</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ URL::to('src/js/checkout.js') }}"></script>
@endsection

