@extends('layouts.master')

@section('title')
    Corn'er' Games!
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="row">
            <div id="charge-message" class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif
    @foreach($products->chunk(3) as $productChunk)
        <div class="row">
            @foreach($productChunk as $product)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="{{ $product->imagePath}}"  class="img-responsive" alt="...">
                        <div class="caption">
                            <h3>{{ $product->title }}</h3>
                            <p class="description">{{ $product->description }}</p>
                            <div>
                                <div class="price">{{ $product->price }} PLN</div>
                                <div class="card-button">
                                    <a href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn btn-success" role="button">Dodaj do koszyka</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach




@endsection

