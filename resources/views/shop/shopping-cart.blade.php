@extends('layouts.master')

@section('title')
    Corn'er' Games!
@endsection

@section('content')
        <div id="sc-container">
            @if(Session::has('cart'))
                @foreach($products as $product)
                    <div class="row" style="margin: auto; background-color: #2c2c2d; border-radius: 20px">
                        <ul class="shopping-cart">
                            <li class="shopping-cart-item" style="height:100%">
                                <span class="badge" style="font-size: 20px; height: 95%">{{ $product['qty'] }} szt.</span>
                                <strong style="font-size: 30px; height: 95%">{{ $product['item']['title'] }}</strong>
                                <span class="badge badge-dark" style="font-size: 25px">{{ $product['price'] }} PLN</span>
                                <div class="btn-group" style="height: 95%; padding-top: 20px">
                                    <div class="dropdown" style="height: 95%">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Edytuj
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="sc-dropdown-menu">
                                            <li><a href="{{ route('product.reduceByOne', ['id' => $product['item']['id']]) }}">Zmniejsz o 1</a></li>
                                            <div class="dropdown-divider"></div>
                                            <li><a href="{{ route('product.remove', ['id' => $product['item']['id']]) }}">Usuń wszystkie</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <hr style="border: 0.5px solid #330066;">
                        </ul>
                    </div>
                @endforeach
                <hr style="border: 2px solid #330066;">
                <div class="row">
                    <div id="sc-total">
                        <strong>Suma: {{ $totalPrice }} PLN</strong>
                    </div>
                </div>
                <div class="row">
                    <div id="div-sc-button">
                        <a href="{{ route('checkout') }}" role="button" class="btn btn-success" id="sc-button">Podsumowanie</a>
                    </div>
                </div>
            @else
                <div style="margin-left: auto; margin-right: auto">
                    <div class="row" style="margin: auto; color: #bfbfbf">
                        <h2>Brak produktów w koszyku!</h2>
                    </div>
                </div>

            @endif
        </div>

@endsection