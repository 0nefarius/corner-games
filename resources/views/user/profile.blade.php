@extends('layouts.master')

@section('title')
    Corn'er' Games!
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12" style="width: 80%; margin-left: auto; margin-right: auto">
            <h1 style="color: #bfbfbf; margin-bottom: 30px; margin-top: 30px">Moje zamówienia:</h1>
            <hr>
            @foreach($orders as $order)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-group" >
                            @foreach($order->cart->items as $item)
                                <li class="list-group-item" style="background-color: #2c2c2d; color: #bfbfbf">
                                    <span class="badge badge-dark">{{ $item['price'] }} PLN</span>
                                    {{ $item['item']['title'] }}  |  {{ $item['qty'] }} szt.
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="panel-footer" style="font-size: 30px; color: #bfbfbf; margin-top: 10px; margin-bottom: 20px; border-bottom: 1px solid #7300e6">
                        <strong>Suma: {{ $order->cart->totalPrice }} PLN</strong>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection