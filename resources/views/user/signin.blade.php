@extends('layouts.master')

@section('title')
    Corn'er' Games!
@endsection

@section('content')
    <div class="row">
        <div id="sign-in">
            <h1>Wejdź na swoje konto!</h1><br/>
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('user.signin') }}" method="post">
                <div class="form-group">
                    <input type="text" placeholder="E-mail" id="email" name="email">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Hasło" id="password" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Zaloguj się!</button>
                {{ csrf_field() }}
            </form>
            <p>Nie masz jeszcze konta? <a href="{{ route('user.signup') }}">Zarejestruj się!</a></p>
        </div>
    </div>
@endsection